
# coding: utf-8

# In[1]:


"""Training program for SRGAN implementation.

Benchmark data sets provided by the paper are available here: https://twitter.app.box.com/s/lcue6vlrd01ljkdtdkhmfvk7vtjhetog
"""
import tensorflow as tf
tf.InteractiveSession()
from tensorflow.python.training import queue_runner
import numpy as np
import argparse
import srgan#Luis as srgan
from benchmark import Benchmark
import os
import sys
# sys.add(../)
from utilitiesLuis import downsample_batch, build_log_dir, preprocess, evaluate_model #, build_inputs,  test_examples

from memory_profiler import profile

# ### MAIN 

# In[2]:


# parser = argparse.ArgumentParser()
# parser.add_argument('--load', type=str, help='Checkpoint to load all weights from.')
# parser.add_argument('--load-gen', type=str, help='Checkpoint to load generator weights only from.')
# parser.add_argument('--name', type=str, help='Name of experiment.')
# parser.add_argument('--overfit', action='store_true', help='Overfit to a single image.')
# parser.add_argument('--batch-size', type=int, default=16, help='Mini-batch size.')
# parser.add_argument('--log-freq', type=int, default=10000, help='How many training iterations between validation/checkpoints.')
# parser.add_argument('--learning-rate', type=float, default=1e-4, help='Learning rate for Adam.')
# parser.add_argument('--content-loss', type=str, default='mse', choices=['mse', 'vgg22', 'vgg54'], help='Metric to use for content loss.')
# parser.add_argument('--use-gan', action='store_true', help='Add adversarial loss term to generator and trains discriminator.')
# parser.add_argument('--image-size', type=int, default=96, help='Size of random crops used for training samples.')
# parser.add_argument('--vgg-weights', type=str, default='vgg_19.ckpt', help='File containing VGG19 weights (tf.slim)')
# parser.add_argument('--train-dir', type=str, help='Directory containing training images')
# parser.add_argument('--validate-benchmarks', action='store_true', help='If set, validates that the benchmarking metrics are correct for the images provided by the authors of the SRGAN paper.')
# parser.add_argument('--gpu', type=str, default='0', help='Which GPU to use')
# args = parser.parse_args()

# os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu


load = '' #D:\\UPC\\git\\tensorflow-SRGAN-trevor\\checkpoints\\'                        #help='Checkpoint to load all weights from.'
load_gen='' #D:\\UPC\\git\\tensorflow-SRGAN-trevor\\checkpoints\\generator\\'                      
name='V5'
overfit=False
batch_size=8
log_freq=20
learning_rate=1e-5
content_loss='mse'
use_gan=False
image_size=16
vgg_weights='' #'vgg19.ckpt'
train_dir='/imatge/lsalgueiro/git/gans/srgan-working/data/'
logs='/imatge/lsalgueiro/git/gans/srgan-working/logs/'+name
input_name='Tile_WV2_8K16x16_3B_NCHW.npy'
target_name='Tile_Sent2_8K16x16_3B_NCHW.npy'
validate_benchmarks=False
# gpu=0

max_iter = 10
# os.environ['CUDA_VISIBLE_DEVICES'] = gpu



# In[3]:

#@profile
def build_inputs(overfit, train_dir, batch_size, image_size, target_name, input_name, sess):
    if overfit:
        # Overfit to a single image
        train_filenames = np.array(['overfit.png'])
        val_filenames = np.array(['overfit.png'])
        eval_filenames = np.array(['overfit.png'])
        #args.batch_size = 1
        num_test = 1
    else:
        # Regular dataset
        train_filenames = np.load(train_dir+input_name)
        val_filenames = np.load(train_dir+target_name)
    
    print("Train shape: ", train_filenames.shape)
    print("Val shape: ", val_filenames.shape)
    
    num_images= train_filenames.shape[0]
    print(num_images)
    
    if train_filenames.shape[1]<=4:
        print("Transposing data to format NHWC")
        train_filenames = tf.transpose(train_filenames, [0, 2, 3, 1])
        val_filenames = tf.transpose(val_filenames, [0, 2, 3, 1])
    
    print("Paso 0")
    print(train_filenames)
    get_train_batch, get_val_batch  = tf.train.shuffle_batch([train_filenames, val_filenames],
                                                             batch_size=batch_size,
                                                             num_threads=1,
                                                             capacity=4 * batch_size,
                                                             min_after_dequeue=2 * batch_size,
                                                             enqueue_many=True)
    
    print("..........................................................batch done")
    return get_train_batch, get_val_batch, num_images


# In[4]:


# Set up models
d_training = tf.placeholder(tf.bool, name='d_training')
g_training = tf.placeholder(tf.bool, name='g_training')
discriminator = srgan.SRGanDiscriminator(training=g_training, image_size=image_size)
generator = srgan.SRGanGenerator(discriminator=discriminator, training=d_training, learning_rate=learning_rate, content_loss=content_loss, use_gan=use_gan,num_upsamples=0)


# In[5]:


# Generator
g_x = tf.placeholder(tf.float32, [None, None, None, 3], name='input_lowres')
g_y = tf.placeholder(tf.float32, [None, None, None, 3], name='input_highres')

g_y_pred = generator.forward(g_x)
g_loss = generator.loss_function(g_y, g_y_pred)
g_train_step = generator.optimize(g_loss)

tf.summary.scalar('G_Loss', g_loss)


# Discriminator
d_x_real = tf.placeholder(tf.float32, [None, None, None, 3], name='input_real')
d_y_real_pred, d_y_real_pred_logits = discriminator.forward(d_x_real)
d_y_fake_pred, d_y_fake_pred_logits = discriminator.forward(g_y_pred)
d_loss = discriminator.loss_function(d_y_real_pred, d_y_fake_pred, d_y_real_pred_logits, d_y_fake_pred_logits)
d_train_step = discriminator.optimize(d_loss)
 


# In[6]:


# Set up benchmarks
benchmarks = [Benchmark('Benchmarks/Set5', name='Set5'), 
              Benchmark('Benchmarks/Set14', name='Set14'), 
              Benchmark('Benchmarks/BSD100', name='BSD100')]
if validate_benchmarks:
    for benchmark in benchmarks:
        benchmark.validate()


# In[7]:


# Create log folder
if load and not name:
    log_path = os.path.dirname(load)
else:
    log_path = build_log_dir(name, sys.argv)


### memory watch
tfconfig = tf.ConfigProto(allow_soft_placement=True,
                          device_count={'GPU':1})
tfconfig.gpu_options.allow_growth = True

# In[ ]:


with tf.Session(config=tfconfig) as sess:
    
    # WRITER 
    train_writer = tf.summary.FileWriter( logs, sess.graph)
    
    # Build input pipeline
    get_train_batch, get_val_batch, num_images = build_inputs(overfit, train_dir, batch_size, image_size, target_name, input_name, sess)# (args, sess)
    
    # Initialize
    sess.run(tf.local_variables_initializer())
    sess.run(tf.global_variables_initializer())
    
    # Start input pipeline thread(s)
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)
    
    # Load saved weights
    iteration = 0
    saver = tf.train.Saver()
    # Load generator
    if load_gen:
        print("Ingreso a load_gen")
        gen_saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='generator'))
        iteration = int(load_gen.split('-')[-1])
        gen_saver.restore(sess, load_gen)
    # Load all
    if load:
        print("Ingreso a load....")
        iteration = int(load.split('-')[-1])
        saver.restore(sess, load)
    # Load VGG
    if 'vgg' in content_loss:
        print("Ingreso a vgg...")
        vgg_saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='vgg_19'))
        vgg_saver.restore(sess, vgg_weights)
        

    # Train
    while iteration<max_iter:
        merge = tf.summary.merge_all()

        if iteration % log_freq == 0:
            print(iteration)
	    @profile
            print("Ingreso a Iteracion>>  PASO 0")
            ## Test every log-freq iterations
#             val_error = evaluate_model(g_loss, get_val_batch, sess, num_images, batch_size)
#             eval_error = evaluate_model(g_loss, get_eval_batch, sess, num_images, batch_size)
            ## Log error
#             print('[%d] Test: %.7f' % (iteration, val_error), end='')
            ## Evaluate benchmarks
            log_line = ''
#             for benchmark in benchmarks:
#                 psnr, ssim, _, _ = benchmark.evaluate(sess, g_y_pred, log_path, iteration)
#                 print(' [%s] PSNR: %.2f, SSIM: %.4f' %( benchmark.name, psnr, ssim), end='')
#                 log_line += ',%.7f, %.7f' %(psnr, ssim)
            ## Write to log
#             with open(log_path + '/loss.csv', 'a') as f:
#                 f.write('%d, %.15f, %.15f%s\n' % (iteration, val_error,  log_line))
            
            ## Save checkpoint
            saver.save(sess, os.path.join(log_path, 'weights'), global_step=iteration, write_meta_graph=False)
            
            ##train discriminator
        print("Paso >>> 1 ")
    
        if use_gan:
            print("Ingreso a TRAIN DISCRIMINATOR...")
            batch_hr = sess.run(get_train_batch)
            batch_lr = downsample_batch(batch_hr, factor=4)
            batch_lr, batch_hr = preprocess(batch_lr, batch_hr)
            sess.run(d_train_step, feed_dict={d_training: True, g_training: True, g_x: batch_lr, g_y: batch_hr, d_x_real: batch_hr})
      
            ## Train generator
        print("Ingreso a TRAIN GENERATOR...")
        batch_hr, batch_lr = sess.run([get_train_batch, get_val_batch])
        print(get_train_batch[0])
#       batch_lr = downsample_batch(batch_hr, factor=1)
        batch_lr, batch_hr = preprocess(batch_lr, batch_hr)
        print("Hizo el preprocess")
        summary,_ = sess.run([ merge, g_train_step], feed_dict={d_training: True, g_training: True, g_x: batch_lr, g_y: batch_hr})
        train_writer.add_summary(summary, iteration)

        print("Hizo el run train...")    
        iteration += 1
        print(iteration)
    
    # Stop queue threads
    coord.request_stop()
    coord.join(threads)

